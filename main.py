from bs4 import BeautifulSoup
import urllib2
import pandas as pd

titles = []
companies = []
locations = []
links = []


# get source code of the page
def get_url(url):
    return urllib2.urlopen(url).read()


# makes the source tree format like
def beautify(url):
    source = get_url(url)
    return BeautifulSoup(source, "html.parser")


jobs = beautify('https://www.linkedin.com/jobs/search/?geoId=92000001&keywords=Developpeur&location=%C3%80%20distance')

results_context = jobs.find('div', {'class': 'jobs-search-results'}).find('occludable-update')
n_jobs = int(results_context.text.replace(',', ''))
print "###### Number of job postings #######"
print n_jobs
print "#####################################"

results = jobs.find_all('li', {'class': 'job-listing'})
n_postings = len(results)
print "#### Number of job postings per page ####"
print n_postings
print "#########################################"

print "#### Number of pages ####"
n_pages = int(round(n_jobs / float(n_postings)))
print n_pages
print "#########################"

for i in range(n_pages):
    url = "https://www.linkedin.com/jobs/search/?geoId=92000001&keywords=Developpeur&location=%C3%80%20distance"
url = url.replace('nPostings', str(25 * i))
soup = beautify(url)
# Build lists for each type of information
results = soup.find_all('li', {'class': 'job-listing'})
results.sort()
# print "there are ", len(results) , " results"
for res in results:
    # set only the value if get_text()
    titles.append(res.h2.a.span.get_text() if res.h2.a.span else 'None')
    companies.append(res.find('span', {'class': 'company-name-text'}).get_text() if
                     res.find('span', {'class': 'company-name-text'}) else 'None')
    locations.append(res.find('span', {'class': 'job-location'}).get_text() if
                     res.find('span', {'class': 'job-location'}) else 'None')
    links.append(res.find('a', {'class': 'job-title-link'}).get('href'))

jobs_linkedin = pd.DataFrame({'title' : titles, 'company': companies, 'location': locations, 'link' : links})
jobs_linkedin.count()
